### [Enconsulta](https://enconsulta.onrender.com/)

Sitio web de enconsulta.

### Requisitos

- [Visual Studio Code](https://code.visualstudio.com/download)
- Extensión [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) para Visual Studio Code

### Configuración inicial

1. Descarga o clona este repositorio en tu máquina local:

```bash
git clone https://gitlab.com/jesau7/enconsulta
```

2.  Abre el proyecto en Visual Studio Code.
3.  Instala la extensión Live Server si aún no la tienes. Puedes hacerlo buscando "Live Server" en el Marketplace de extensiones de Visual Studio Code o visitando el [enlace directo](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer).

### Ejecución de la Página Web

1.  Abre el archivo `index.html` o el archivo HTML principal de tu proyecto.
2.  Haz clic con el botón derecho del ratón y selecciona "Open with Live Server". Esto abrirá tu página web en tu navegador predeterminado. 

Nota: No hacer modificaciones porque esto se actualizará automáticamente cada vez que se haga cambios en los archivos del proyecto.

### Base de Datos

Una vez instalado, al correr, la aplicación web, estará conectada directamente con la base de datos en línea y funcionando.

### En sesión

La clave de adminstrador es, usuario: admin; clave: admin. Eso dará acceso a los datos y consultas.

### Correo

Por ser un sistema montado en una sitio de pruebas gratuito, no maneja el correo directamente, por lo tanto se utiliza un emulador de función... https://mailtrap.io/signin pueden acceder desde el enlace. El usuario es: enconsultasalud@gmail.com y la clave: Enconsulta123.
Allí llegan los mensajes de consulta y la respuesta a la cita, la cual, muestra el paso a seguir para realizar la confirmación de la misma.

PD: Cuando esté en línea con dominio pago, podrá recibir la información directo al correo del usuario.




