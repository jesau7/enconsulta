var captcha_response = "";
function useFetch(url = "", method = "GET", data = undefined, options = {}) {
  return new Promise(async function (resolve, reject) {
    try {
      const response = await fetch(`https://enconsulta-api.onrender.com/${url}`, {
        method,
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          Accept: "*/*",
          ...options
        },
      })
      const json = await response.json()
      const statusCode = response.status
      if ([200, 201, 202, 204].includes(statusCode))
        resolve(json)
      else if([401, 422].includes(statusCode)) {
        vanillaToast.show("La sesión ha finalizado", {
          className: "error",
          duration: 3000,
          fadeDuration: 500,
        })
        // setTimeout(() => window.location.href = "login.html", 2000)
      }
      else {
        vanillaToast.show("Error al procesar la solicitud", {
          className: "error",
          duration: 3000,
          fadeDuration: 500,
        })
      }
    } catch (e) {
      console.log({ e })
      reject(e)
    }
  })
}

function getAccessTokenOption() {
  const access_token = localStorage.getItem("access_token");
  return { 'Authorization': `Bearer ${access_token}` }
}

function getSpecialists() {
  return useFetch(`specialists`)
}

function getSlots() {
  return useFetch(`slots`)
}

function getAppoinments() {
  return useFetch(`medical_appoinments`, `GET`, undefined, getAccessTokenOption())
}

function getSpecialities() {
  return useFetch(`specialties`)
}

function getAdmins() {
  return useFetch(`admins`, `GET`, undefined, getAccessTokenOption())
}
async function login() {
  const username = document.querySelector("#username").value
  const password = document.querySelector("#password").value
  const response = await useFetch(`login`, `POST`, { username, password })
  if (response.code < 0) {
    vanillaToast.show("Error al iniciar sesión", {
      className: "error",
      duration: 3000,
      fadeDuration: 500,
    })
  } else {
    vanillaToast.show("Sesión iniciada", {
      className: "success",
      duration: 3000,
      fadeDuration: 500,
    })

    localStorage.setItem("access_token", response.access_token)

    const menuOptions = await getMenuOptions(response.access_token)
    localStorage.setItem("menu_options", JSON.stringify(menuOptions))

    window.location.href = "appoinments.html"
  }
}

async function saveAppoinments() {
  const fullname = document.getElementById("name").value
  const pacient_email = document.getElementById("email").value
  const pacient_phone = document.getElementById("phone").value
  const slot_id = document.getElementById("slots").value
  const specialist_id = document.getElementById("specialist_list").value
  const message = document.getElementById("textarea_message").value
  const appointment_date = document.getElementById("appointment_date").value

  if(!captcha_response) {
      vanillaToast.show("Debe rellenar el Captcha", {
        className: "error",
        duration: 3000,
        fadeDuration: 500,
      })
      return
  }

  if (
    fullname &&
    pacient_email &&
    slot_id &&
    specialist_id &&
    message &&
    appointment_date &&
    pacient_phone
  ) {
    const regex_email = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
    if (!regex_email.test(pacient_email)) {
      vanillaToast.show("Email no valido", {
        className: "error",
        duration: 3000,
        fadeDuration: 500,
      })
      return
    }

    const regex_phone = /^(0414|0424|0412|0416|0426|0212|0241)[0-9]{7}$/g
  if (!regex_phone.test(pacient_phone)) {
    vanillaToast.show("Telefono no valido", {
      className: "error",
      duration: 3000,
      fadeDuration: 500,
    })
    return
  }

    const app_date = moment(appointment_date)

    if(moment().diff(app_date) >= 1){
      vanillaToast.show("Solo puede solicitar citas el dia de mañana o en adelante", {
        className: "error",
        duration: 3000,
        fadeDuration: 500,
      })
      return
    }

    const appoinment_day = moment(appointment_date)

    if(['Sunday', 'Saturday'].includes(appoinment_day.format("dddd"))){
      vanillaToast.show("Solo es posible agendar cita de Lunes a Viernes", {
        className: "error",
        duration: 3000,
        fadeDuration: 500,
      })
      return
    }

    appoinment_day.locale("es")

    const data = {
      appointment_date,
      user_id: 1,
      specialist_id,
      slot_id,
      clinic_id: 1,
      status: "NOT_CONFIRMED",
      fullname,
      appoinment_day: appoinment_day.format("dddd"),
      message,
      pacient_email,
      pacient_phone,
    }

    const response = await useFetch(`medical_appoinments`, `POST`, data)

    if (response.code < 0) {
      vanillaToast.show("Ya existe una cita en este espacio de tiempo", {
        className: "error",
        duration: 3000,
        fadeDuration: 500,
      })
      return
    }

    if (response) {
      vanillaToast.show("Cita Enviada", {
        className: "success",
        duration: 3000,
        fadeDuration: 500,
      })

      document.getElementById("name").value = ""
      document.getElementById("email").value = ""
      document.getElementById("phone").value = ""
      document.getElementById("slots").value = ""
      document.getElementById("specialist_list").value = ""
      document.getElementById("appointment_date").value = new Date()
      document.getElementById("textarea_message").value = ""
      captcha_response = ""
      grecaptcha.reset(widgetId1)
      grecaptcha.reset(widgetId2)
    }
  } else {
    vanillaToast.show("Complete todos los campos", {
      className: "error",
      duration: 3000,
      fadeDuration: 500,
    })
  }
}

async function openModal(id) {
  const button = document.getElementById("addButton")
  await findSpecialist(id)
  button.click()
}

async function openAdminModal(id) {
  const button = document.getElementById("addButton")
  await findAdmin(id)
  button.click()
}

async function findSpecialist(id) {
  const response = await useFetch(`specialists/${id}`, `GET`, undefined, getAccessTokenOption())
  if (response) {
    document.querySelector("#id").value = response.id
    document.querySelector("#name").value = response.name
    document.querySelector("#lastname").value = response.lastname
    document.querySelector("#email").value = response.email
    document.querySelector("#gender").value = response.gender
    document.querySelector("#title").value = response.title
    document.querySelector("#phone").value = response.phone
    document.querySelector("#ci_document").value = response.ci_document
    document.querySelector("#speciality_list").value = response.speciality_id
  }
}

async function findAdmin(id) {
  const response = await useFetch(`admins/${id}`, `GET`, undefined, getAccessTokenOption())
  if (response) {
    document.querySelector("#id").value = response.id
    document.querySelector("#username").value = response.username
    document.querySelector("#password").value = response.password
  }
}
async function saveSpecialist() {
  const id = document.querySelector("#id").value
  const name = document.querySelector("#name").value
  const lastname = document.querySelector("#lastname").value
  const email = document.querySelector("#email").value
  const gender = document.querySelector("#gender").value
  const title = document.querySelector("#title").value
  const phone = document.querySelector("#phone").value
  const ci_document = document.querySelector("#ci_document").value
  const speciality_id = document.querySelector("#speciality_list").value

  if (
    !(
      ci_document &&
      name &&
      lastname &&
      email &&
      gender &&
      title &&
      phone &&
      speciality_id
    )
  ) {
    vanillaToast.show("Complete todos los campos", {
      className: "error",
      duration: 3000,
      fadeDuration: 500,
    })
    return
  }

  const regex_email = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
  if (!regex_email.test(email)) {
    vanillaToast.show("Email no valido", {
      className: "error",
      duration: 3000,
      fadeDuration: 500,
    })
    return
  }

  const regex_phone = /^(0414|0424|0412|0416|0426|0212|0241)[0-9]{7}$/g
  if (!regex_phone.test(phone)) {
    vanillaToast.show("Telefono no valido", {
      className: "error",
      duration: 3000,
      fadeDuration: 500,
    })
    return
  }

  let method = "POST"
  let uri = "specialists"

  const _id = parseInt(id)

  if (!isNaN(_id)) {
    data = { ...data, id }
    method = "PUT"
    uri = "specialists/" + id
  }

  const response = await useFetch(`${uri}`, method, data, getAccessTokenOption())
  if (response) {
    vanillaToast.show("Especialista Guardado", {
      className: "success",
      duration: 3000,
      fadeDuration: 500,
    })
    clearForm()
    document.location.href = "specialists.html"
  } else {
    vanillaToast.show("Error al guardar", {
      className: "error",
      duration: 3000,
      fadeDuration: 500,
    })
  }
}

async function saveAdmin() {
  const id = document.querySelector("#id").value
  const username = document.querySelector("#username").value
  const password = document.querySelector("#password").value
  const role_id = document.querySelector("#role_id").value

  if (!(username && password)) {
    vanillaToast.show("Complete todos los campos", {
      className: "error",
      duration: 3000,
      fadeDuration: 500,
    })
    return
  }

  let data = {
    id,
    username,
    password,
    role_id,
  }

  let method = "POST"
  let uri = "admins"

  const _id = parseInt(id)

  if (!isNaN(_id)) {
    data = { ...data, id }
    method = "PUT"
    uri = "admins/" + id
  }

  const response = await useFetch(`${uri}`, method, data, getAccessTokenOption())
  if (response) {
    vanillaToast.show("Administrador Guardado", {
      className: "success",
      duration: 3000,
      fadeDuration: 500,
    })
    clearForm()
    document.location.href = "admins.html"
  } else {
    vanillaToast.show("Error al guardar", {
      className: "error",
      duration: 3000,
      fadeDuration: 500,
    })
  }
}

function clearForm() {
  const close = document.querySelector("#modal-exit")
  close.click()
  const id = document.querySelector("#id")
  if(id) id.value = null
  const name = document.querySelector("#name")
  if(name) name.value = null
  const lastname = document.querySelector("#lastname")
  if(lastname) lastname.value = null
  const email = document.querySelector("#email")
  if(email) email.value = null
  const gender = document.querySelector("#gender")
  if(gender) gender.value = null
  const title = document.querySelector("#title")
  if(title) title.value = null
  const phone = document.querySelector("#phone")
  if(phone) phone.value = null
  const ci_document = document.querySelector("#ci_document")
  if(ci_document) ci_document.value = null
  const specialist_list = document.querySelector("#speciality_list")
  if(specialist_list) specialist_list.value = null

  const username = document.querySelector("#username")
  if(username) username.value = null

  const password = document.querySelector("#password")
  if(password) password.value = null
}

async function deleteSpecialist(id) {
  if (confirm("¿Está seguro de eliminar este registro?")) {
    const response = await useFetch(`specialists/${id}`, `DELETE`, undefined, getAccessTokenOption())
    if (response) {
      vanillaToast.show("Especialista Eliminado", {
        className: "success",
        duration: 3000,
        fadeDuration: 500,
      })
      document.location.href = "specialists.html"
    }
  }
}

async function deleteAdmin(id) {
  if (confirm("¿Está seguro de eliminar este registro?")) {
    const response = await useFetch(`admins/${id}`, `DELETE`, undefined, getAccessTokenOption())
    if (response) {
      vanillaToast.show("Administrador Eliminado", {
        className: "success",
        duration: 3000,
        fadeDuration: 500,
      })
      document.location.href = "admins.html"
    }
  }
}

async function deleteAppointment(id) {
  if (confirm("¿Está seguro de eliminar este registro?")) {
    const response = await useFetch(`medical_appoinments/${id}`, `DELETE`, undefined, getAccessTokenOption())
    if (response) {
      vanillaToast.show("Cita Eliminada", {
        className: "success",
        duration: 3000,
        fadeDuration: 500,
      })
      document.location.href = "appoinments.html"
    }
  }
}

async function sendEmai() {
  const message = document.querySelector("#email_message").value
  const from_email = document.querySelector("#patient_email").value
  const name = document.querySelector("#email_name").value
  const phone = document.querySelector("#phone_email").value
  const subject = document.querySelector("#subject").value

  if(!captcha_response) {
      vanillaToast.show("Debe rellenar el Captcha", {
        className: "error",
        duration: 3000,
        fadeDuration: 500,
      })
      return
  }

  if (!(message && from_email && name && email && phone && subject)) {
    vanillaToast.show("Complete todos los campos", {
      className: "error",
      duration: 3000,
      fadeDuration: 500,
    })
    return
  }

  const regex_email = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
  if (!regex_email.test(from_email)) {
    vanillaToast.show("Email no valido", {
      className: "error",
      duration: 3000,
      fadeDuration: 500,
    })
    return
  }

  const regex_phone = /^(0414|0424|0412|0416|0426|0212|0241)[0-9]{7}$/g
  if (!regex_phone.test(phone)) {
    vanillaToast.show("Telefono no valido", {
      className: "error",
      duration: 3000,
      fadeDuration: 500,
    })
    return
  }

  const data = {
    message,
    from_email,
    name,
    phone,
    subject,
  }

  const response = await useFetch(`send-message`, `POST`, data)
  if (response) {
    vanillaToast.show("Email Enviado", {
      className: "success",
      duration: 3000,
      fadeDuration: 500,
    })

    document.querySelector("#email_message").value = ""
    document.querySelector("#patient_email").value = ""
    document.querySelector("#email_name").value = ""
    document.querySelector("#phone_email").value = ""
    document.querySelector("#subject").value = ""
    captcha_response = ""
    grecaptcha.reset(widgetId1)
    grecaptcha.reset(widgetId2)
  }
}

// function to patch medical_appoinments by ID
async function patchMedicalAppoinment(id) {
  const data = {
    status: "CONFIRMED",
  }
  try {
    const response = await useFetch(`medical_appoinments/${id}`, `PATCH`, data, getAccessTokenOption())
    if (response.code < 0) {
      document.querySelector("#message").innerHTML =
        "No se ha podido confirmar la cita"
      return
    }
    if (response) {
      vanillaToast.show("Cita confirmada", {
        className: "success",
        duration: 3000,
        fadeDuration: 500,
      })
      document.querySelector("#message").innerHTML = "Cita confirmada"
    }
  } catch (err) {
    console.log(err)
    vanillaToast.show("Error al confirmar cita", {
      className: "success",
      duration: 3000,
      fadeDuration: 500,
    })
    document.querySelector("#message").innerHTML =
      "No se ha podido confirmar la cita"
  }
}

function decodeJWT(token) {
  const parts = token.split('.');
  
  if (parts.length !== 3) {
      throw new Error('Token format is invalid');
  }
  
  const header = JSON.parse(atob(parts[0]));
  const payload = JSON.parse(atob(parts[1]));
  
  return { header, payload };
}

async function getMenuOptions (token) {
  const { payload } = decodeJWT(token)
  const menuOptions = await useFetch(`/menu_options/${payload.sub.role}`, `GET`, undefined, getAccessTokenOption())
  return menuOptions;
}

function isSecretary () {
  const token = localStorage.getItem("access_token")
  const { payload } = decodeJWT(token)
  return payload.sub.role === 'secretary'
}

function generateMenu(active = "") {
  const navElement = document.getElementById('menu');
  const menuObject = JSON.parse(localStorage.getItem('menu_options'));

  menuObject.menu_options.forEach(function(menuOption) {
      const liElement = document.createElement('li');
      
      const aElement = document.createElement('a');
      aElement.href = menuOption.url;  // Establece el atributo href
      aElement.textContent = menuOption.name;  // Establece el texto del enlace
      if(active === menuOption.name) {
        liElement.classList.add('active');
      }
      
      liElement.appendChild(aElement);
      navElement.appendChild(liElement);
  });
}